# README #

### Mistä simulaatiossa on kyse? ###

Tämä ohjelma simuloi tiettyjä evolutiivisia prosesseja: suuntaavaa valintaa sekä geneettistä ajautumista (genetic drift). Suvullisesti lisääntyvillä värikkäillä nelikulmioilla on genomi, joka säätelee niiden väriä, muotoa ja liikkumista. Nämä ominaisuudet muuttuvat populaatiossa ohjelman edetessä.

Suuntaava valinta

Pelissä on zombi, punainen neliö, joka syö värikkäitä "ihmisiä" osuessaan niihin. Isot ja nopeasti liikkuvat "ihmiset" ovat todennäköisempiä saaliita, joten pienempi koko ja hitaampi liikkuminen yleistyy.

Geneettinen ajautuminen

"Ihmisten" väriin ei kohdistu minkäänlaista valintaa, mutta niiden värit yhtenäistyvät jossain määrin ohjelman edetessä. Tämä johtuu genetic drift -ilmiöstä.
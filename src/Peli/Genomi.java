package Peli;

import java.awt.Color;
import java.util.Random;

public class Genomi {

    private int[] a1;
    private int[] a2;
    Color edellinenVari;

    public Genomi() {
        Random r = new Random();
        a1 = new int[256];
        a2 = new int[256];
        for (int i = 0; i < 256; i++) {
            a1[i] = r.nextInt(256);
            a2[i] = r.nextInt(256);
        }
    }

    public Genomi(Genomi aiti, Genomi isa) {
        a1 = aiti.getLapsiKromosomi(aiti.a1, aiti.a2);
        a2 = isa.getLapsiKromosomi(isa.a1, isa.a2);
        mutaatioita();
    }

    private int[] getLapsiKromosomi(int[] kromosomi1, int[] kromosomi2) {
        int[] x = new int[kromosomi1.length];
        Random r = new Random();
        for (int i = 0; i < kromosomi1.length; i++) {
            int random = r.nextInt(2);
            if (random == 0) {
                x[i] = kromosomi1[i];
            } else {
                x[i] = kromosomi2[i];
            }
        }
        return x;

    }

    private int get(int i) {
        if (a1[i] < a2[i]) {
            return a1[i];
        }
        return a2[i];
    }

    public Color getTaustavari() { //lokukset 2-20
        Random r = new Random();
        int[] vari = {get(2) + (int) (r.nextGaussian() * get(3) / 3), get(4) + (int) (r.nextGaussian() * get(5) / 3), get(6) + (int) (r.nextGaussian() * get(7) / 3)};
        for (int i = 0; i < 3; i++) {
            if (vari[i] < 0) {
                vari[i] = 0;
            }
            if (vari[i] > 255) {
                vari[i] = 255;
            }
        }
        if (edellinenVari == null) {
            edellinenVari = new Color(vari[0], vari[1], vari[2]);
        }
        Color uusiVari = painotettuVari(new Color(vari[0], vari[1], vari[2]), get(8), get(9), get(10));
        edellinenVari = uusiVari;
        return uusiVari;
    }

    private Color painotettuVari(Color color, int i, int j, int k) {
        int r = (int) (1.0 * i / 256 * color.getRed() + ((256 - i * 1.0) / 256) * edellinenVari.getRed());
        int g = (int) (1.0 * j / 256 * color.getGreen() + ((256 - j * 1.0) / 256) * edellinenVari.getGreen());
        int b = (int) (1.0 * k / 256 * color.getBlue() + ((256 - k * 1.0) / 256) * edellinenVari.getBlue());
        return new Color(r, g, b);
    }

    public int getVauhti() { //lokukset 21-25
        int vauhti = (get(21) / 15);
        if (vauhti % 2 == 0) {
            vauhti += 1;
        }
        return vauhti;
    }

    public int getKorkeus() { //koolle lokukset 26-30
        // TODO Auto-generated method stub
        return ((get(26) + get(27) + get(28)) / 3 + 1);
    }

    public int getLeveys() {
        return ((get(26) + get(29) + get(30)) / 3 + 1);
    }

    public double getSuunta(int liikemoodi) { //31-35
        return get(31 + liikemoodi);
    }

    public int getLiikemaara(int liikemoodi) { //36-40
        return (get(36 + liikemoodi));
    }

    private void mutaatioita() { //41-42
        mutaatioita(a1, get(41));
        mutaatioita(a2, get(42));
    }

    private void mutaatioita(int[] kromosomi, int kuinkaMonta) { // TODO säädä vielä
        Random r = new Random();
        for (int i = 0; i < kuinkaMonta / 12; i++) {
            kromosomi[r.nextInt(256)] = r.nextInt(256);
        }
    }
}

package Peli;

import java.awt.Image;
import java.util.Random;

public class Humanoidi {

	protected ZombiesWillEatYou peli;
	protected Image kuva = null;
	protected int x;
	protected int y;
	protected static Random r = new Random();
	
	protected int pystyliike;
	protected int sivuliike;
	
	protected int vauhti;
	
	protected int red;
	protected int green;
	protected int blue;
	
	protected int korkeus;
	protected int leveys;
	
	protected double suunta =666;
	protected int liikemaara = 10;
	
	public Humanoidi (ZombiesWillEatYou peli) {
		this.peli = peli;
		vauhti = 5; //pitää ehkä olla pariton?
		red = 0;
		green = 0;
		blue = 0;
	}
	
	public Image getImage() {
		return kuva;
	}

	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getX2() {
		return x + kuva.getWidth(peli);
	}
	
	public int getY2() {
		return y + kuva.getHeight(peli);
	}
	
	public void vaihdaSuuntaa(double x) {
		suunta += x;
		while (suunta >= (2*Math.PI)) {
			suunta -= (2*Math.PI);
		}
	}
	
	public void liiku () {
		if (suunta == 666) {
			x += sivuliike;
			y += pystyliike;
		}
		else {
			pystyliike = (int) ((Math.cos(suunta))*vauhti);
			sivuliike = (int) ((Math.sin(suunta))*vauhti);
			x += sivuliike;
			y += pystyliike;
			liikemaara --;
			if (liikemaara == 0) {
				paataUusiSuunta();
				liikemaara = 10;
				
			}
		}
	}

	public void paataUusiSuunta() {
		
	}

	public void pidaRuudulla(int screenWidth, int screenHeight) {
		int raja =0;
		if (y<0 || y> (screenHeight - kuva.getHeight(peli))) {
			while (y+pystyliike<0) {
				y++;
				raja++;
				if (raja>100) break;
			}
			while (y> (screenHeight - kuva.getHeight(peli))) {
				y--;
				raja++;
				if (raja>100) break;
			}
		}
		if (x<0 || x> (screenWidth - kuva.getWidth(peli))) {
			x -= sivuliike;
		}
	}
	public void pelastaRuudunUlkopuolelta(int screenWidth, int screenHeight) {
		if (getY2()> screenHeight) {
			y = (screenHeight-korkeus);
		}
		if (getX2()> screenWidth) {
			x = (screenWidth-leveys);
		}
	}
	public boolean onkoSisalla(Humanoidi humanoidi) {
		if (
				(x > humanoidi.getX() && x < humanoidi.getX2()
				&&  y > humanoidi.getY() && y < humanoidi.getY2()) ||
				(x > humanoidi.getX() && x < humanoidi.getX2()
				&&  getY2() > humanoidi.getY() && getY2() < humanoidi.getY2())||
				(getX2() > humanoidi.getX() && getX2() < humanoidi.getX2()
				&&  y > humanoidi.getY() && y < humanoidi.getY2())||
				(getX2() > humanoidi.getX() && getX2() < humanoidi.getX2()
				&&  getY2() > humanoidi.getY() && getY2() < humanoidi.getY2())||
				(x<humanoidi.getX() && getX2()>humanoidi.getX2()&& y>humanoidi.getY() && getY2()<humanoidi.getY2())||
				(x>humanoidi.getX() && getX2()<humanoidi.getX2()&& y<humanoidi.getY() && getY2()>humanoidi.getY2()))
		{
			return true;
		}
		return false;
	}
	public void kuole () {
		peli.kuole(this);
	}
}
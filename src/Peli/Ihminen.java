package Peli;

import java.awt.Graphics;
import java.util.Random;

public class Ihminen extends Humanoidi {

	Genomi genomi;
	int liikemoodi; //(0-4)
	static Random r = new Random(); 
	
	public Ihminen(ZombiesWillEatYou peli) {
		super(peli);
		x = peli.getIhmistenAlkuX();
		y = peli.getIhmistenAlkuY();
		genomi = new Genomi();
		vauhti = genomi.getVauhti();
		korkeus = genomi.getKorkeus();
		leveys = genomi.getLeveys();
		suunta = genomi.getSuunta(liikemoodi);
		teeKuva();
		
		
	}
	public Ihminen(ZombiesWillEatYou peli, Ihminen aiti, Ihminen isa) {
		super(peli);
		x = aiti.getX();
		y = aiti.getY();
		genomi = new Genomi(aiti.genomi, isa.genomi);
		vauhti = genomi.getVauhti();
		korkeus = genomi.getKorkeus();
		leveys = genomi.getLeveys();
		suunta = genomi.getSuunta(liikemoodi);
		teeKuva();
	}
	/*double kuinkaKiinnostava(Ihminen ihminen) {
		return genomi.kuinkaKiinnostava(this, x, y, ihminen, ihminen.getX(), ihminen.getY2());
	}
	
	double  kuinkaKiinnostava(Zombi zombi) {
		return genomi.kuinkaKiinnostava(this, x, y, zombi, zombi.getX(), zombi.getY2());
	} */

	void teeKuva () {
		leveys = genomi.getLeveys();
		korkeus = genomi.getKorkeus();
		kuva = peli.createImage(leveys, korkeus);
		Graphics g = kuva.getGraphics();
		g.setColor(genomi.getTaustavari());
		g.fillRect(0, 0, leveys, korkeus);
		for (int i =0; i<leveys; i ++) {
			for (int j=0; j<korkeus; j++) {
				g.setColor(genomi.getTaustavari());
				g.drawRect(i, j, i, j);
				//System.out.println(i + " " + j + " " + genomi.getTaustavari().getBlue());
			}
		} 
	}
	@Override
	public void paataUusiSuunta() {
		liikemoodi = r.nextInt(5);
		vaihdaSuuntaa(genomi.getSuunta(liikemoodi));
		liikemaara = genomi.getLiikemaara(liikemoodi);
	}


}

package Peli;

public class Vect {
	double x;
	double y;
	public Vect (int nykyX, int nykyY, int paamaaraX, int paamaaraY) {
		y = 1.0*(paamaaraY-nykyY);
		x = 1.0*(paamaaraX-nykyX);
		double kerroin = (Math.sqrt(x*x+y*y));
		x = x/kerroin;
		y = y/kerroin;
	}
}
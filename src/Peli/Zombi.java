package Peli;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Random;

public class Zombi extends Humanoidi {
	
	int kohdeX;
	int kohdeY;
	
	int hupikokeilu;
	private boolean haluaaUudenKohteen = true;
	
	public Zombi (ZombiesWillEatYou peli) {
		
		super(peli);
		x = peli.getZombienAlkuX();
		y = peli.getZombienAlkuY();
		korkeus = 20;
		leveys = 20;
		kuva = peli.createImage(korkeus, leveys);
		Graphics g = kuva.getGraphics();
		g.setColor(Color.RED);
		g.fillRect(0, 0, korkeus, leveys);
	}

	public void annaKohde(int x, int y) {
		if (haluaaUudenKohteen) {
		kohdeX=x;
		kohdeY=y;
		if (kohdeX>peli.screenWidth-leveys) {
			kohdeX -= leveys;
		}
		if (kohdeY>peli.screenHeight-korkeus) {
			kohdeY -= korkeus;
		}
		}
		haluaaUudenKohteen = false;
	}
	 
	public void liiku() {
		Vect suuntavektori = new Vect(x, y, kohdeX, kohdeY);
		//System.out.println(x + " " + y + " " + kohdeX + " " + kohdeY + " " + suunta);
		if ((Math.abs(x - kohdeX)>(vauhti/2+2) || Math.abs(y- kohdeY)>(vauhti/2+2))) {
			pystyliike = (int) (vauhti*suuntavektori.y);
			sivuliike = (int) (vauhti*suuntavektori.x);
		}
		else {
			haluaaUudenKohteen = true;
		}
		super.liiku();
	}
}
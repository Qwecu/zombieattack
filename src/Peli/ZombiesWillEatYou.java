package Peli;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JApplet;

import javax.swing.Timer;


public class ZombiesWillEatYou extends JApplet implements KeyListener, ActionListener {

  int screenWidth;
  int screenHeight;

  Timer timer;

  Image ammus;
  Image scarlett;
  
  private int zombienAlkuX;
  private int zombienAlkuY;
  
  private int ihmistenAlkuX;
  private int ihmistenAlkuY;
  
  int zombejaAlussa = 1;
  int ihmisiaAlussa = 25;
  int ihmisiaPelinAikana = 70;
  int alkuihmisiaLuotu;
  
  ArrayList<Zombi> zombit = new ArrayList<Zombi>();
  ArrayList<Ihminen> ihmiset = new ArrayList<Ihminen>();

  Image backbuffer;
  Graphics backGraphics;

  int lastTimeWeWereInActionPerformed;

  int ammusX;

  boolean aDown;

  public void init() {
    timer = new Timer(15, this);
    timer.start();

    scarlett       = getImage(getCodeBase(), "images.jpg");

    screenWidth  = 1100;
    screenHeight = 600; 

    setSize(screenWidth, screenHeight);
    setBackground(Color.black);
    
    zombienAlkuX = (screenWidth/3);
    zombienAlkuY = (screenHeight/2);
    
    ihmistenAlkuX = (2*screenWidth/3);
    ihmistenAlkuY = (screenHeight/2);
    
    luoZombit();

    backbuffer = createImage(screenWidth, screenHeight);
    backGraphics = backbuffer.getGraphics();

    addKeyListener(this);
  }


  	private void luoZombit() {
		for (int i = 0; i<zombejaAlussa; i++) {
			zombit.add(new Zombi(this));
		}
		alkuihmisiaLuotu = 0;
  	}


public void update(Graphics g) {
    backGraphics.setColor(Color.black);
    backGraphics.fillRect(0, 0, screenWidth, screenHeight);
    
    for(Ihminen ihminen : ihmiset) {
    	backGraphics.drawImage(ihminen.getImage(), ihminen.getX(), ihminen.getY(), this);
    }
    for (Zombi zombi : zombit) {
    	backGraphics.drawImage(zombi.getImage(), zombi.getX(), zombi.getY(), this);
    }

    g.drawImage(backbuffer, 0, 0, this);
  }

  public void paint(Graphics g) {
    update(g);
  }


  public void actionPerformed(ActionEvent e) {

	if (alkuihmisiaLuotu < ihmisiaAlussa) {
		ihmiset.add(new Ihminen(this));
		alkuihmisiaLuotu++;
	}
	else if (ihmiset.size()>0 && ihmiset.size()<ihmisiaPelinAikana) {
		Random r = new Random();
		Ihminen aiti = ihmiset.get(r.nextInt(ihmiset.size()));
		Ihminen isa = ihmiset.get(r.nextInt(ihmiset.size()));
		if (isa != aiti) ihmiset.add(new Ihminen(this, aiti, isa));
	}
    for (Zombi z : zombit) {
    	if (ihmiset.size() !=0) {
    		int i = (new Random()).nextInt(ihmiset.size());
    	z.annaKohde(ihmiset.get(i).getX(), ihmiset.get(i).getY());
    	}
    	z.liiku();
    	z.pidaRuudulla(screenWidth, screenHeight);
    }
    for(Ihminen z : ihmiset) {
    	z.liiku();
    	z.pidaRuudulla(screenWidth, screenHeight);
    	z.pelastaRuudunUlkopuolelta(screenWidth, screenHeight);
    }
    
    ArrayList<Ihminen> kuolleet = new ArrayList<Ihminen>();
    
    for (Zombi z : zombit) {
    	for (Ihminen ih: ihmiset) {
    		if (z.onkoSisalla(ih)||ih.onkoSisalla(z)) {
    			kuolleet.add(ih);
    		}
    	}
    }

    for (Ihminen ih: kuolleet) { ih.kuole(); }
    
    repaint();
  }


  public void keyTyped(KeyEvent evt) {}

  public void keyPressed(KeyEvent evt) {
    char c = evt.getKeyChar();
    if (c == 'a') aDown = true;
  }
  public void keyReleased(KeyEvent evt) {
    char c = evt.getKeyChar();
    if (c == 'a') aDown = false;
  }

	public int getZombienAlkuX() {
		return zombienAlkuX;
	}
	
	public int getZombienAlkuY() {
		return zombienAlkuY;
	}
	public int getIhmistenAlkuX() {
		return ihmistenAlkuX;
	}
	
	public int getIhmistenAlkuY() {
		return ihmistenAlkuY;
	}


	public void kuole(Humanoidi h) {
		ihmiset.remove(h);
	}
}
